<?php

error_reporting(E_ALL & ~E_STRICT);
ini_set('display_errors', '1');

if (!function_exists('oci_connect')) {
    header("HTTP/1.0 502 Bad Gateway");
    die("function oci_connect not defined\n");
}

$sql_details = array(
	"type" => NULL,         // Database type: "Mysql", "Postgres", "Sqlserver", "Sqlite" or "Oracle"
	"user" => "",   		// Database user name
	"pass" => "",           // Database password (TBS)
	"host" => NULL, 		// Database host
	"port" => NULL,         // Database connection port (can be left empty for default)
	"db"   => "",           // Database name (dev: devdb19; test: int8r; prod: atlr)
	"dsn"  => "",           // PHP DSN extra information. Set as `charset=utf8mb4` if you are using MySQL
	"pdoAttr" => array()   	// PHP PDO attributes array. See the PHP documentation for all options
);

$sql_details["type"] = getenv("ORACLE_DB_TYPE");
if (!$sql_details["type"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("ORACLE_DB_TYPE not defined\n");
}

$sql_details["user"] = getenv("ORACLE_DB_USER");
if (!$sql_details["user"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("ORACLE_DB_USER not defined\n");
}

$sql_details["pass"] = getenv("ORACLE_DB_PASSWORD");
if (!$sql_details["pass"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("ORACLE_DB_PASSWORD not defined\n");
}

$sql_details["db"] = getenv("ORACLE_DB");
if (!$sql_details["db"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("ORACLE_DB not defined\n");
}

echo "<DL>\n";
echo "<DT>type</DT>\n";
echo "<DD>".$sql_details["type"]."</DD>\n";
echo "<DT>user</DT>\n";
echo "<DD>".$sql_details["user"]."</DD>\n";
echo "<DT>db</DT>\n";
echo "<DD>".$sql_details["db"]."</DD>\n";
echo "</DL>\n";

$otpConnection = oci_connect($sql_details["user"], $sql_details["pass"], $sql_details["db"]);
if (!$otpConnection) {
    header("HTTP/1.0 502 Bad Gateway");
    $m = oci_error();
    if ($m) {
        echo $m['message'], "\n";
    }
    exit;
}

$sql = 'SELECT * FROM PUB_SYSTEM_NODE';
$stid = oci_parse($otpConnection, $sql);
if (!$stid) {
    header("HTTP/1.0 502 Bad Gateway");
    die("'$sql' not valid\n");
}

oci_execute($stid);

echo "<table border='1'>\n";
while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
    echo "<tr>\n";
    foreach ($row as $item) {
        echo "    <td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
    }
    echo "</tr>\n";
}
echo "</table>\n";
