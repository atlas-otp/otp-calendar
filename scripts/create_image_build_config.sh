#!/bin/sh

oc delete bc php-oracle
cat php-oracle/Dockerfile | oc new-build --image-stream=php:8.0-ubi8 --name='php-oracle' --strategy=docker --dockerfile=-
