# A FROM line must be present but is ignored. It will be overridden by the --image-stream parameter in the BuildConfig
FROM registry.access.redhat.com/ubi8/php-80

# TODO(4) this is the list of packages to install. Modify as needed. In this example, we install the Oracle client 19.21 and CERN tnsnames.ora configuration file.
ARG EXTRA_PACKAGES="oracle-instantclient19.21-basic oracle-instantclient19.21-devel https://otp-calendar-atlas.app.cern.ch/oracle-instantclient-tnsnames.ora-1.4.8-1.al8.cern.noarch.rpm"
# "https://linuxsoft.cern.ch/internal/repos/dbclients8al-stable/x86_64/os/Packages/oracle-instantclient-tnsnames.ora-1.4.8-1.al8.cern.noarch.rpm"
# The following installs relevant CERN AlmaLinux8 repos for Oracle drivers and EPEL. More repos can be added if needed.
ARG EXTRA_REPOS="https://linuxsoft.cern.ch/cern/alma/8/CERN/x86_64/Packages/e/epel-release-8-19.al8.cern.noarch.rpm https://linuxsoft.cern.ch/cern/alma/8/CERN/x86_64/Packages/o/oracle-release-1.5-2.al8.cern.noarch.rpm"

ARG OUR_PACKAGES="diffutils php-devel php-pear nano"

# Temporarily switch to root user to install packages
USER root

RUN : \
# use CERN repository mirrors as per https://gitlab.cern.ch/linuxsupport/ubi8
&& sed -i 's#://cdn-ubi.redhat.com/#://linuxsoft.cern.ch/cdn-ubi.redhat.com/#' /etc/yum.repos.d/ubi.repo \
# switch to AlmaLinux8 release as per https://linux.web.cern.ch/migration/
&& :> /etc/dnf/protected.d/redhat-release.conf && :> /etc/yum.repos.d/redhat.repo && dnf --repofrompath='tmpcern,https://linuxsoft.cern.ch/cern/alma/8/CERN/x86_64/' swap -y redhat-release almalinux-release --nogpgcheck \
# GPG keys for Oracle packages
&& rpm --import https://linuxsoft.cern.ch/mirror/yum.oracle.com/RPM-GPG-KEY-oracle-ol8 \
# Install desired repos & packages
&& dnf install -y ${EXTRA_REPOS} \
&& dnf install -y ${EXTRA_PACKAGES} \
&& dnf install -y ${OUR_PACKAGES} \
&& dnf clean all

ENV ORACLE_HOME /usr/lib/oracle/19.21/client64/lib
ENV LD_LIBRARY_PATH /usr/lib/oracle/19.21/client64/lib:${LD_LIBRARY_PATH}

# connect to php
RUN wget --no-check-certificate https://pecl.php.net/get/oci8-3.0.1.tgz
RUN tar -zxf oci8-3.0.1.tgz
RUN (cd oci8-3.0.1 ; phpize ; ./configure -with-oci8=shared,instantclient,/usr/lib/oracle/19.21/client64/lib ; make install ; echo extension=oci8.so >> /etc/php.d/30-oracle.ini)
RUN rm -fr oci8-3.0.1.tgz oci8-3.0.1

# RUN pecl install oci8-3.0.1 --with-oci8=instantclient,/usr/lib/oracle/19.21/client64/lib

# Make sure the final image runs as unprivileged user
USER 1001
