<?php
// redirect to opt-next to get a username
$debug = isset($_REQUEST['debug']);

$server_name = $_SERVER['SERVER_NAME'];

$redirect_host = "not.available";
if ($server_name == "otp-calendar-atlas.app.cern.ch") {
    $redirect_host = "otp-next-atlas.app.cern.ch";
} elseif ($server_name == "otp-calendar-atlas-alpha.app.cern.ch") {
    $redirect_host = "otp-next-atlas-alpha.app.cern.ch";
}

$edit_query = $_REQUEST;
// unset($edit_query['xxx']);
$query_string = http_build_query($edit_query);

$url = "https://$redirect_host/otp-calendar/index.php?$query_string";

if ($debug) {
    header('Content-Type: text/html');
    echo "<pre>\n";
    echo "Server Name: $server_name\n";
    echo "Query String: $query_string\n";
    echo "Redirect to <a href=\"$url\">$url</a>\n";
    echo "<pre>\n";
    die();
}

header("Status: 301 Moved Permanently");
header("Location: $url");
die("Get a username");
