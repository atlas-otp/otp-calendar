<?php

function end_date($end_date, $hf, $ht) {
    if ($ht <= $hf) {
        $end_date = $end_date->modify('+1 day');
    }
    return $end_date;
}

function convert($date, $hf, $ht) {
    $start_date = DateTimeImmutable::createFromFormat('d-M-y', $date);
    $end_date = end_date($start_date, $hf, $ht);
    echo $start_date->format("d-M-y"), " ", $end_date->format("d-M-y"), " ", $hf, " ", $ht, "\n";
}

convert('22-MAY-23', 7, 15);
convert('22-MAY-23', 23, 7);

convert('31-MAY-23', 7, 15);
convert('31-MAY-23', 23, 7);

convert('31-DEC-23', 7, 15);
convert('31-DEC-23', 23, 7);

convert('04-MAY-22', 10, 10);
