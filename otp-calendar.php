<?php
error_reporting(E_ALL & ~E_STRICT);
ini_set('display_errors', '1');

require('vendor/autoload.php');

use Eluceo\iCal\Domain\Entity\Calendar;
use Eluceo\iCal\Domain\Entity\Event;
use Eluceo\iCal\Domain\Enum\EventStatus;
use Eluceo\iCal\Domain\ValueObject\Alarm;
use Eluceo\iCal\Domain\ValueObject\Category;
use Eluceo\iCal\Domain\ValueObject\GeographicPosition;
use Eluceo\iCal\Domain\ValueObject\Location;
use Eluceo\iCal\Domain\ValueObject\TimeSpan;
use Eluceo\iCal\Domain\ValueObject\UniqueIdentifier;
use Eluceo\iCal\Domain\ValueObject\Uri;
use Eluceo\iCal\Presentation\Component\Property;
use Eluceo\iCal\Presentation\Component\Property\Value\DurationValue;
use Eluceo\iCal\Presentation\Component\Property\Value\TextValue;
use Eluceo\iCal\Presentation\Factory\CalendarFactory;

class CustomCalendarFactory extends CalendarFactory {
    protected function getProperties(Calendar $calendar): Generator {
        global $uid, $years, $category_codes;

        yield from parent::getProperties($calendar);
        yield new Property('METHOD', new TextValue('PUBLISH'));
        yield new Property('X-WR-CALNAME', new TextValue("ATLAS Shifts $years $category_codes"));
        yield new Property('X-WR-RELCALID', new TextValue($uid."-0"));
        yield new Property('X-PUBLISHED-TTL;VALUE=DURATION', new DurationValue(new DateInterval('P1D')));
        yield new Property('REFRESH-INTERVAL;VALUE=DURATION', new DurationValue(new DateInterval('P1D')));
    }
}

function end_date($end_date, $hour_from, $hour_to) {
    if ($hour_to <= $hour_from) {
        $end_date = $end_date->modify('+1 day');
    }
    return $end_date;
}

function get_uid($uuid, $years, $category_codes, $node) {
    // The UID should be unique for the calendar (seq=0) and any events (seq>0). Mac Calendar can only subscribe to each
    // calendar UID once (per calender type [google, icloud...]). The query_string w.o the uuid and debug should be encoded.
    // The sequence number is appended afterwards to make debugging easy
    $uid = "$uuid-$years-$category_codes@$node";
    $md5 = hash_hmac('md5', $uid, "secret");
    return $md5;
}

$text = "";

if (!isset($_REQUEST['uuid'])) {
    header("HTTP/1.0 502 Bad Gateway");
    die("No UUID specified\n");
}
$uuid = $_REQUEST['uuid'];

$debug = isset($_REQUEST['debug']);
$years = isset($_REQUEST['years']) ? $_REQUEST['years'] : date("Y");
$category_codes = '';
$class1 = 'Class 1';
$class2 = 'Class 2';
if (isset($_REQUEST['category_code'])) {
    switch ($_REQUEST['category_code']) {
        case 'Class 1':
            $class2 = null;
            $category_codes = 'Class 1';
            break;
        case 'Class 2':
            $class1 = null;
            $category_codes = 'Class 2';
            break;
        default:
            // ignore
            break;
    }
}

// Constants
$node = $_SERVER['SERVER_NAME'];
$product_id = "-//$node/ical//2.0/EN";
$name = "ATLAS-OTP";
$uid = get_uid($uuid, $years, $category_codes, $node);
$task_url_prefix = "https://atlas-otp.cern.ch/mao/client/cern.ppt.mao.app.gwt.MaoClient/MaoClient.html#Ma0_Task_panel(N";
$task_url_postfix = ")";
$timezone = "Europe/Paris";
$point1 = (new Location('CERN Point 1 - ATLAS'))->withGeographicPosition(new GeographicPosition(46.23548, 6.05576));
$not_point1 = (new Location('CERN General - ATLAS'))->withGeographicPosition(new GeographicPosition(46.231177, 6.054721));
$home_institute = new Location('Home Institute - ATLAS');

// Database
if (!function_exists('oci_connect')) {
    header("HTTP/1.0 502 Bad Gateway");
    die("function oci_connect not defined\n");
}

$sql_details = array (
	"type" => NULL,         // Database type: "Mysql", "Postgres", "Sqlserver", "Sqlite" or "Oracle"
	"user" => "",   		// Database user name
	"pass" => "",           // Database password (TBS)
	"host" => NULL, 		// Database host
	"port" => NULL,         // Database connection port (can be left empty for default)
	"db"   => "",           // Database name (dev: devdb19; test: int8r; prod: atlr)
	"dsn"  => "",           // PHP DSN extra information. Set as `charset=utf8mb4` if you are using MySQL
	"pdoAttr" => array()   	// PHP PDO attributes array. See the PHP documentation for all options
);

$sql_details["type"] = getenv("ORACLE_DB_TYPE");
if (!$sql_details["type"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("ORACLE_DB_TYPE not defined\n");
}

$sql_details["user"] = getenv("ORACLE_DB_USER");
if (!$sql_details["user"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("ORACLE_DB_USER not defined\n");
}

$sql_details["pass"] = getenv("ORACLE_DB_PASSWORD");
if (!$sql_details["pass"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("ORACLE_DB_PASSWORD not defined\n");
}

$sql_details["db"] = getenv("ORACLE_DB");
if (!$sql_details["db"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("ORACLE_DB not defined\n");
}

$otpConnection = oci_connect($sql_details["user"], $sql_details["pass"], $sql_details["db"]);
if (!$otpConnection) {
    header("HTTP/1.0 502 Bad Gateway");
    $m = oci_error();
    if ($m) {
        echo $m['message'], "\n";
    }
    exit;
}

// FIXME RID and ORDER
$sql =
"SELECT * FROM PUB_CALENDAR
where uuid = :uuid
and year in (SELECT column_value FROM table(:years))
and category_code in ('$class1', '$class2')";
// and RID <> 555265";
// order by RID, SHIFT_TYPE, DT";

$stid = oci_parse($otpConnection, $sql);
if (!$stid) {
    header("HTTP/1.0 502 Bad Gateway");
    die("Request not valid\n");
}

oci_bind_by_name($stid, ":uuid", $uuid);

// Maximum length of collections of type ODCINUMBERLIST is 32767, maybe you should check that!
$years_collection = oci_new_collection($otpConnection, 'ODCINUMBERLIST', 'SYS');
foreach (explode(',', $years) as $year) {
    $years_collection->append($year);
}
oci_bind_by_name($stid, ':years', $years_collection, -1, SQLT_NTY);

oci_execute($stid);

$calendar = new Calendar();
$calendar->setProductIdentifier($product_id);

$shift = new Category("Shifts");

$seq = 1;
$event = null;
$prev_rid = null;
$prev_shift_type = null;
$concatenate = true;

while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
    // print_r($row);
    $tid = $row['TID'];
    $task = $row['TASK'];
    $rid = $row['RID'];
    $requirement = $row['REQUIREMENT'];
    $shift_type = $row['SHIFT_TYPE'];
    $dt = $row['DT'];
    $hour_from = $row['HOUR_FROM'];
    $hour_to = $row['HOUR_TO'];

    $dt_start = DateTimeImmutable::createFromFormat('d-M-y e', $dt." ".$timezone);
    $dt_end = end_date($dt_start, $hour_from, $hour_to);

    $dt_start = $dt_start->setTime($hour_from, 0);
    $dt_end = $dt_end->setTime($hour_to, 0);

    $location = null;
    switch ($row['LOCATION']) {
        case 'Point1':
            $location = $point1;
            break;
        case 'NotPoint1':
            $location = $not_point1;
            break;
        case 'HomeInstitute':
            $location = $home_institute;
            break;
        case null:
            $location = new Location("No Location");
            break;
        default:
            $location = new Location($row['LOCATION']);
            break;
    }

    if ($event != null) {
        if ($concatenate) {
            // Update end datetime
            if (($rid == $prev_rid) && ($shift_type == $prev_shift_type) && ($dt_start == $event->getOccurrence()->getEnd()->getDateTime())) {
                $dt_start = $event->getOccurrence()->getBegin()->getDateTime();
                $event->setOccurrence(new TimeSpan(
                    new Eluceo\iCal\Domain\ValueObject\DateTime($dt_start, true),
                    new Eluceo\iCal\Domain\ValueObject\DateTime($dt_end, true)));
                // $text .= "new start and end time for $seq: \n";
                // $text .= $event->getOccurrence()->getBegin()->getDateTime()->format('d-M-y e H:i')."\n";
                // $text .= $event->getOccurrence()->getEnd()->getDateTime()->format('d-M-y e H:i')."\n";
                continue;
            }
        }

        $calendar->addEvent($event);
        $seq++;
    }

    $event = (new Event(new UniqueIdentifier($uid."-".$seq)))
        ->setSummary($task)
        ->setDescription($requirement)
        ->setLocation($location)
        ->setUrl(new Uri("$task_url_prefix$tid$task_url_postfix"))
        ->addCategory($shift)
        ->setOccurrence(new TimeSpan(
            new Eluceo\iCal\Domain\ValueObject\DateTime($dt_start, true),
            new Eluceo\iCal\Domain\ValueObject\DateTime($dt_end, true)))
        ->addAlarm(
            new Alarm(
                new Alarm\DisplayAction('Reminder: your shifts starts tomorrow'),
                (new Alarm\RelativeTrigger(DateInterval::createFromDateString('-1 day')))->withRelationToStart()
            )
        )
        ->setStatus(EventStatus::CONFIRMED());
    $prev_rid = $rid;
    $prev_shift_type = $shift_type;
}

if ($event != null) {
    $calendar->addEvent($event);
    $seq++;
}

// $text .= "Seq: $seq\n";

// Transform domain entity into an iCalendar component
$componentFactory = new CustomCalendarFactory();
$calendarComponent = $componentFactory->createCalendar($calendar);

// Set headers
if ($debug) {
    header('Content-Type: text/plain');
    echo $text;
} else {
    header('Content-Type: text/calendar; charset=utf-8');
    header('Content-Disposition: attachment; filename="atlas-shifts.ics"');
}

// Output
echo $calendarComponent;
